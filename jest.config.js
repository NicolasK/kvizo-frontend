const { defaults } = require('jest-config');

module.exports = {
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'jsx'],
  setupFilesAfterEnv: ['<rootDir>tests/setupTests.js'],
  moduleNameMapper: {
    '^.*[.](jpe?g|svg|gif|png|less|css|scss)$': '<rootDir>/tests/assetsMock.js',
  },
};
