import React from 'react';

import classNames from 'classnames';

import './goodBadAnswerPill.scss';

export function GoodBadAnswerPill(props) {
  let isRight = props.row.userAnswer === props.row.rightAnswer;

  function renderTooltip() {
    if (!isRight)
      return (
        <span className="sheet tooltip">
          Bonne réponse :
          <span className="right-answer">{props.row.rightAnswer}</span>
        </span>
      );
    else return null;
  }

  return (
    <span
      className={classNames('answer-pill', { right: isRight, wrong: !isRight })}
    >
      {renderTooltip()}
    </span>
  );
}
