import React, { useState } from 'react';

import './app.scss';

import { Quiz } from './Quiz';
import { QuizMenu } from './QuizMenu';

export function App() {
  let [currentQuiz, setCurrentQuiz] = useState(null);

  const QUIZ = [
    {
      id: 1,
      title: 'Les capitales (facile)',
      questions: [
        {
          id: 0,
          statement: 'Quelle est la capitale de la France ?',
          answers: [
            { value: 'Paris', correct: true },
            { value: 'Madrid' },
            { value: 'Luxembourg' },
          ],
        },
        {
          id: 1,
          statement: 'Quelle est la capitale de la Belgique ?',
          answers: [
            { value: 'Bruxelles', correct: true },
            { value: 'Paris' },
            { value: 'Londres' },
          ],
        },
        {
          id: 2,
          statement: 'Quelle est la capitale de la Finlande ?',
          answers: [
            { value: 'Helsinki', correct: true },
            { value: 'Madrid' },
            { value: 'Vienne' },
          ],
        },
        {
          id: 3,
          statement: 'Quelle est la capitale de l’Autriche ?',
          answers: [
            { value: 'Vienne', correct: true },
            { value: 'Oslo' },
            { value: 'Bruxelles' },
          ],
        },
        {
          id: 4,
          statement: 'Quelle est la capitale de l’Espagne ?',
          answers: [
            { value: 'Bruxelles' },
            { value: 'Madrid', correct: true },
            { value: 'Paris' },
          ],
        },
        {
          id: 5,
          statement: 'Quelle est la capitale de la Suède ?',
          answers: [
            { value: 'Oslo' },
            { value: 'Helsinki' },
            { value: 'Stockholm', correct: true },
          ],
        },
        {
          id: 6,
          statement: 'Quelle est la capitale de la Norvège ?',
          answers: [
            { value: 'Oslo', correct: true },
            { value: 'Helsinki' },
            { value: 'Stockholm' },
          ],
        },
      ],
    },
    {
      id: 2,
      title: 'Les capitales (moyen)',
      questions: [
        {
          id: 0,
          statement: 'Quelle est la capitale de l’Estonie ?',
          answers: [
            { value: 'Tallinn', correct: true },
            { value: 'Riga' },
            { value: 'Vilnius' },
          ],
        },
        {
          id: 1,
          statement: 'Quelle est la capitale de la Lituanie ?',
          answers: [
            { value: 'Tallinn' },
            { value: 'Riga' },
            { value: 'Vilnius', correct: true },
          ],
        },
        {
          id: 2,
          statement: 'Quelle est la capitale de la Lettonie ?',
          answers: [
            { value: 'Tallinn' },
            { value: 'Riga', correct: true },
            { value: 'Vilnius' },
          ],
        },
        {
          id: 3,
          statement: 'Quelle est la capitale de la Serbie ?',
          answers: [
            { value: 'Belgrade', correct: true },
            { value: 'Pristina' },
            { value: 'Sarajevo' },
          ],
        },
        {
          id: 4,
          statement: 'Quelle est la capitale du Kosovo ?',
          answers: [
            { value: 'Belgrade' },
            { value: 'Pristina', correct: true },
            { value: 'Sarajevo' },
          ],
        },
        {
          id: 5,
          statement: 'Quelle est la capitale de la Bosnie Herzégovine ?',
          answers: [
            { value: 'Belgrade' },
            { value: 'Pristina' },
            { value: 'Sarajevo', correct: true },
          ],
        },
      ],
    },
  ];

  function setCurrentQuizFromId(id) {
    setCurrentQuiz(QUIZ.filter(quiz => quiz.id === id)[0]);
  }

  if (currentQuiz === null) {
    return (
      <QuizMenu allQuiz={QUIZ} onChoose={quiz => setCurrentQuizFromId(quiz)} />
    );
  } else {
    return (
      <Quiz
        shuffle
        title={currentQuiz.title}
        questions={currentQuiz.questions}
        onQuit={() => setCurrentQuiz(null)}
      />
    );
  }
}
