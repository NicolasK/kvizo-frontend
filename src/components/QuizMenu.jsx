import React from 'react';
import PropTypes from 'prop-types';

import './quizMenu.scss';

export function QuizMenu(props) {
  function renderMenu() {
    return props.allQuiz.map(quiz => (
      <button key={quiz.title} onClick={() => props.onChoose(quiz.id)}>
        {quiz.title}
      </button>
    ));
  }

  return (
    <div className="menu">
      <h1>Quiz</h1>
      <nav>{renderMenu()}</nav>
    </div>
  );
}

QuizMenu.propTypes = {
  allQuiz: PropTypes.array.isRequired,
  onChoose: PropTypes.func.isRequired,
};
