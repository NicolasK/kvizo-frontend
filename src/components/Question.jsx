import React from 'react';
import PropTypes from 'prop-types';

import './question.scss';
import { shuffle } from '../utils/arrayUtils';

export class Question extends React.Component {
  renderAnswer(answer) {
    let handleClick = answer.correct
      ? this.props.onRightAnswer
      : this.props.onWrongAnswer;

    return (
      <button
        key={answer.value}
        name={answer.value}
        className="answer"
        onClick={() => handleClick(answer.value)}
      >
        {answer.value}
      </button>
    );
  }

  renderAnswers() {
    let answers = this.props.answers;
    if (this.props.shuffle) {
      answers = shuffle(answers);
    }
    return answers.map(answer => this.renderAnswer(answer));
  }

  render() {
    return (
      <div className="sheet question-container">
        <div className="question-content">
          <div className="question">
            <p>{this.props.statement}</p>
          </div>
          {this.renderAnswers()}
        </div>
      </div>
    );
  }
}

Question.propTypes = {
  statement: PropTypes.string.isRequired,
  answers: PropTypes.array.isRequired,
  onRightAnswer: PropTypes.func.isRequired,
  onWrongAnswer: PropTypes.func.isRequired,
  shuffle: PropTypes.bool,
};

Question.defaultProps = {
  onRightAnswer: () => {},
  onWrongAnswer: () => {},
  shuffle: false,
};
