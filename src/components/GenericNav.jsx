import React from 'react';
import PropTypes from 'prop-types';

import './genericNav.scss';

export function GenericNav(props) {
  function renderButton(button) {
    return (
      <button key={button.name} name={button.name} onClick={button.action}>
        {button.label}
      </button>
    );
  }

  function renderButtons() {
    return props.buttons.map(button => renderButton(button));
  }

  return <nav className="genericNav">{renderButtons()}</nav>;
}

GenericNav.propTypes = {
  buttons: PropTypes.array.isRequired,
};
