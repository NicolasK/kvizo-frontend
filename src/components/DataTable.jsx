import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './dataTable.scss';

export class DataTable extends React.Component {
  renderHeaderValue(column) {
    return column.name;
  }

  renderHeaders() {
    return this.props.columns.map(column => (
      <th
        key={column.key}
        className={classNames('bold-small', { sortable: column.sortable })}
      >
        {this.renderHeaderValue(column)}
      </th>
    ));
  }

  renderHeader() {
    return <tr>{this.renderHeaders()}</tr>;
  }

  static renderValue(column, row) {
    if ('component' in column) {
      const MyComponent = column.component;
      return <MyComponent row={row} />;
    }
    return row[column.key];
  }

  renderRow(row) {
    return this.props.columns.map(column => (
      <td key={column.key}>{DataTable.renderValue(column, row)}</td>
    ));
  }

  renderContentRows() {
    if (this.props.data !== null && this.props.data.length > 0) {
      return this.props.data.map(row => (
        <tr key={row['id']}>{this.renderRow(row)}</tr>
      ));
    } else {
      return (
        <tr>
          <td colSpan={this.props.columns.length} className="no-value">
            Aucune valeur
          </td>
        </tr>
      );
    }
  }

  render() {
    return (
      <table className="data-table sheet">
        <thead>{this.renderHeader()}</thead>
        <tbody>{this.renderContentRows()}</tbody>
      </table>
    );
  }
}

DataTable.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
};
