import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Question } from './Question';
import { QuizResult } from './QuizResult';
import { shuffle } from '../utils/arrayUtils';

export function Quiz(props) {
  let [questions] = useState(
    props.shuffle ? shuffle(props.questions) : props.questions
  );
  let [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  let [userAnswers, setUserAnswers] = useState([]);

  function renderQuestion() {
    function addUserAnswer(userAnswer) {
      setCurrentQuestionIndex(currentQuestionIndex + 1);
      setUserAnswers([...userAnswers, userAnswer]);
    }

    let currentQuestion = questions[currentQuestionIndex];
    return (
      <Question
        key={currentQuestion.id}
        statement={currentQuestion.statement}
        answers={currentQuestion.answers}
        onRightAnswer={addUserAnswer}
        onWrongAnswer={addUserAnswer}
        shuffle
      />
    );
  }

  function renderQuizResult() {
    let questionsWithRightAnswers = questions.map(question => ({
      id: question.id,
      statement: question.statement,
      rightAnswer: question.answers.filter(answer => answer.correct)[0].value,
    }));
    return (
      <QuizResult
        questions={questionsWithRightAnswers}
        userAnswers={userAnswers}
        onQuit={props.onQuit}
      />
    );
  }

  if (currentQuestionIndex < questions.length) {
    return renderQuestion();
  } else {
    return renderQuizResult();
  }
}

Quiz.propTypes = {
  questions: PropTypes.array.isRequired,
  shuffle: PropTypes.bool,
  onQuit: PropTypes.func,
};

Quiz.defaultProps = {
  shuffle: false,
};
