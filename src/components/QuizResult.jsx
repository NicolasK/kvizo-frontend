import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './quizResult.scss';

import { formatPercent } from '../utils/formatters';
import { GenericNav } from './GenericNav';
import { DataTable } from './DataTable';
import { GoodBadAnswerPill } from './GoodBadAnswerPill';

export function QuizResult(props) {
  let [seeDetails, setSeeDetails] = useState(false);
  let toggleSeeDetails = () => setSeeDetails(!seeDetails);

  let { questions, userAnswers } = props;

  function renderPercent() {
    let isRightAnswer = (q, index) => q.rightAnswer === userAnswers[index];
    let percent = questions.filter(isRightAnswer).length / questions.length;
    let pClasses = classNames('percent', {
      good: percent >= 0.7,
      bad: percent <= 0.3,
    });
    return <p className={pClasses}>{formatPercent(percent)}</p>;
  }

  function renderDetails() {
    const COLUMNS = [
      { key: 'statement', name: 'Question' },
      { key: 'userAnswer', name: 'Votre réponse' },
      { key: 'rightAnswer', name: 'Points', component: GoodBadAnswerPill },
    ];
    return (
      <DataTable
        data={props.questions.map((question, index) => ({
          id: question.id,
          statement: question.statement,
          userAnswer: props.userAnswers[index],
          rightAnswer: question.rightAnswer,
        }))}
        columns={COLUMNS}
      />
    );
  }

  function renderResult() {
    if (seeDetails) return renderDetails();
    else return renderPercent();
  }

  function renderNav() {
    return (
      <GenericNav
        buttons={[
          {
            name: 'details',
            action: toggleSeeDetails,
            label: seeDetails ? 'Cacher les réponses' : 'Voir les réponses',
          },
          {
            name: 'menu',
            action: props.onQuit,
            label: 'Retour au menu',
          },
        ]}
      />
    );
  }

  return (
    <div className="quiz-result">
      {renderResult()}
      {renderNav()}
    </div>
  );
}

QuizResult.propTypes = {
  questions: PropTypes.array.isRequired,
  userAnswers: PropTypes.array.isRequired,
};
