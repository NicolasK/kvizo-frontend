export function formatPercent(percent) {
  return Math.round(percent * 100) + '%';
}
