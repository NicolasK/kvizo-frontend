import React from 'react';
import { shallow, render } from 'enzyme';
import toJson from 'enzyme-to-json';
import { DataTable } from '../../src/components/DataTable';

describe('<DataTable />', () => {
  const dataTest = [{ id: 0, col1: 7, col2: 42 }, { id: 1, col1: 8, col2: 43 }];
  const columnsTestWithoutComponent = [
    { key: 'col1', name: 'Colonne 1' },
    { key: 'col2', name: 'Colonne 2' },
  ];

  function MyTestComponent(props) {
    return <span>My component has value {props.row['col1']}</span>;
  }
  const columnsTestWithComponent = [
    {
      key: 'col1',
      name: 'Colonne 1',
      component: MyTestComponent,
    },
    {
      key: 'col2',
      name: 'Colonne 2',
      component: MyTestComponent,
    },
  ];

  it('should match snapshot when no component', () => {
    const renderedComponent = shallow(
      <DataTable data={dataTest} columns={columnsTestWithoutComponent} />
    );
    expect(toJson(renderedComponent)).toMatchSnapshot();
  });

  it('should match snapshot when component', () => {
    const renderedComponent = render(
      <DataTable data={dataTest} columns={columnsTestWithComponent} />
    );
    expect(toJson(renderedComponent)).toMatchSnapshot();
  });
});
