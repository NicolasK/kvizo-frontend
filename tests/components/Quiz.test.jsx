import React from 'react';
import { shallow } from 'enzyme';
import { Quiz } from '../../src/components/Quiz';

describe('<Quiz/>', () => {
  const defaultQuestions = [
    {
      id: 1,
      statement: '?',
      answers: [
        { value: 'A1', correct: true },
        { value: 'B1' },
        { value: 'C1' },
      ],
    },
    {
      id: 2,
      statement: '??',
      answers: [
        { value: 'A2', correct: true },
        { value: 'B2' },
        { value: 'C2' },
      ],
    },
  ];

  it('should render a question', () => {
    let quiz = shallow(<Quiz questions={defaultQuestions} />);
    expect(quiz.find('Question')).toHaveLength(1);
  });

  it('should render QuizResult when all questions answered', () => {
    let quiz = shallow(<Quiz questions={defaultQuestions} />);
    quiz
      .find('Question')
      .props()
      .onRightAnswer('A1');
    quiz
      .find('Question')
      .props()
      .onWrongAnswer('B2');

    let quizResult = quiz.find('QuizResult');
    expect(quizResult).toHaveLength(1);
    expect(quizResult.props().questions).toEqual([
      { id: 1, statement: '?', rightAnswer: 'A1' },
      { id: 2, statement: '??', rightAnswer: 'A2' },
    ]);
    expect(quizResult.props().userAnswers).toEqual(['A1', 'B2']);
  });
});
