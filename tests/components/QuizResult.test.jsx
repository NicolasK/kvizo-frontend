import React from 'react';
import { shallow, mount } from 'enzyme';
import { QuizResult } from '../../src/components/QuizResult';

describe('<QuizResult/>', () => {
  let questions = [
    { id: 1, statement: '?', rightAnswer: 'A' },
    { id: 2, statement: '??', rightAnswer: 'A' },
  ];

  it('should render the percent with good class if percent >= 0.7', () => {
    let quizResult = shallow(
      <QuizResult questions={questions} userAnswers={['A', 'A']} />
    );
    expect(quizResult.find('.good')).toHaveLength(1);
    expect(quizResult.find('.good').text()).toEqual('100%');
  });

  it('should render the percent with bad class if percent <= 0.3', () => {
    let quizResult = shallow(
      <QuizResult questions={questions} userAnswers={['B', 'B']} />
    );
    expect(quizResult.find('.bad')).toHaveLength(1);
    expect(quizResult.find('.bad').text()).toEqual('0%');
  });

  it('should render a GenericNav component', () => {
    let quizResult = shallow(
      <QuizResult questions={questions} userAnswers={['A', 'B']} />
    );
    expect(quizResult.find('GenericNav')).toHaveLength(1);
  });

  it('should render a DataTable component when seeDetails state is true', () => {
    let quizResult = mount(
      <QuizResult questions={questions} userAnswers={['A', 'B']} />
    );
    quizResult.find('button[name="details"]').simulate('click');
    expect(quizResult.find('DataTable')).toHaveLength(1);
  });
});
