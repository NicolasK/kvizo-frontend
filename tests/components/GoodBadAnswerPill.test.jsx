import React from 'react';
import { shallow } from 'enzyme';
import { GoodBadAnswerPill } from '../../src/components/GoodBadAnswerPill';

describe('<GoodBadAnswerPill/>', () => {
  it('should render an answer-pill with "right" class when row.rightAnswer equals row.userAnswer', () => {
    let goodBadAnswerPill = shallow(
      <GoodBadAnswerPill row={{ rightAnswer: 'a', userAnswer: 'a' }} />
    );
    expect(goodBadAnswerPill.find('.answer-pill.right')).toHaveLength(1);
  });

  it('should render an answer-pill with "wrong" class when row.rightAnswer does not equal row.userAnswer', () => {
    let goodBadAnswerPill = shallow(
      <GoodBadAnswerPill row={{ rightAnswer: 'a', userAnswer: 'b' }} />
    );
    expect(goodBadAnswerPill.find('.answer-pill.wrong')).toHaveLength(1);
  });

  it('should render a tooltip with right answer when row.rightAnswer does not equal row.userAnswer', () => {
    let goodBadAnswerPill = shallow(
      <GoodBadAnswerPill row={{ rightAnswer: 'a', userAnswer: 'b' }} />
    );
    let rightAnswer = goodBadAnswerPill.find('.tooltip .right-answer');
    expect(rightAnswer).toHaveLength(1);
    expect(rightAnswer.text()).toEqual('a');
  });
});
