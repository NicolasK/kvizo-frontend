import React from 'react';
import { render, shallow } from 'enzyme';

import { Question } from '../../src/components/Question';

describe('<Question/>', () => {
  const defaultProps = {
    statement: 'question?',
    answers: [{ value: 'A', correct: true }, { value: 'B' }, { value: 'C' }],
    onRightAnswer: () => {},
    onWrongAnswer: () => {},
  };

  it('should render all answers as buttons', () => {
    const question = render(<Question {...defaultProps} />);
    expect(question.find('button')).toHaveLength(defaultProps.answers.length);
  });

  it('should call onRightAnswer callback when correct answer clicked', () => {
    let handleRightAnswer = jest.fn(() => {});
    const question = shallow(
      <Question {...defaultProps} onRightAnswer={handleRightAnswer} />
    );
    let correctAnswer = question.find('button[name="A"]');
    correctAnswer.simulate('click');
    expect(handleRightAnswer.mock.calls).toEqual([['A']]);
  });

  it('should call onWrongAnswer callback when wrong answer clicked', () => {
    let handleWrongAnswer = jest.fn(() => {});
    const question = shallow(
      <Question {...defaultProps} onWrongAnswer={handleWrongAnswer} />
    );
    let correctAnswer = question.find('button[name="B"]');
    correctAnswer.simulate('click');
    expect(handleWrongAnswer.mock.calls).toEqual([['B']]);
  });
});
