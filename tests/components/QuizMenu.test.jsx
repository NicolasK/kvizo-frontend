import React from 'react';
import { mount } from 'enzyme';

import { QuizMenu } from '../../src/components/QuizMenu';

describe('<QuizMenu>', () => {
  it('should render all buttons', () => {
    let allQuiz = [
      { id: 1, title: 'A', questions: [] },
      { id: 2, title: 'B', questions: [] },
    ];

    let quizMenu = mount(<QuizMenu allQuiz={allQuiz} onChoose={() => {}} />);
    let buttons = quizMenu.find('button');
    expect(buttons).toHaveLength(2);
    buttons.forEach((button, index) => {
      expect(button.text()).toEqual(allQuiz[index].title);
    });
  });

  it('should call onChoose callback when button clicked', () => {
    let myCallback = jest.fn();
    let allQuiz = [
      { id: 1, title: 'A', questions: [] },
      { id: 2, title: 'B', questions: [] },
    ];

    let quizMenu = mount(<QuizMenu allQuiz={allQuiz} onChoose={myCallback} />);
    quizMenu
      .find('button')
      .at(1)
      .simulate('click');
    expect(myCallback.mock.calls).toEqual([[2]]);
  });
});
