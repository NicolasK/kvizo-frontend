import { shuffle } from '../../src/utils/arrayUtils';

describe('shuffle', () => {
  it('should not change items', () => {
    let array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let shuffledArray = shuffle(array);
    expect(shuffledArray).toHaveLength(array.length);
    array.forEach(item => expect(shuffledArray).toContain(item));
  });

  it('should shuffle items so that each slot statistically contains one particular item as many times as any other slot', () => {
    const N = 10000;
    let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    let statsOf3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for (let i = 0; i < N; ++i) {
      let shuffledArray = shuffle(array);
      statsOf3[shuffledArray.findIndex(item => item === 3)]++;
    }
    statsOf3 = statsOf3.map(number => number / N);
    expect(statsOf3.filter(stat => stat < 0.09 || stat > 1.01)).toHaveLength(0);
  });
});
